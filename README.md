# EXPERIMENTOS DOUTORADO #

Nos vídeos abaixo COM reprojeção das janelas de detecção (bounding boxes - BBs), a reprojeção é utilizada apenas para demonstrar a complementariedade entre as câmeras. Conforme mencionado no texto do trabalho, a reprojeção não é utilizada no processo de controle do robô, mas apenas no processo de eliminação de falsos positivos. Repare ainda que, uma BB só é projetada em outra imagem, se não possuir correspondente (IoU > 0.5) na referida imagem.

# EXPERIMENTO 1 #
# **3.3.2.1 Tarefa de seguimento de seres humanos**

**Vídeo SEM reprojeção das BBs**: [Vídeo 1](https://drive.google.com/open?id=1eOQqYTJWaGzJzJvyLbVF23MRtyrn5jJk);

**Vídeo COM reprojeção das BBs**: [Vídeo 2](https://drive.google.com/open?id=1hO-obIHUwJues26g5IHiS1Vuv1k7G7k3).

# EXPERIMENTO 2 #
# **3.3.2.2 Tarefa de desvio de seres humanos**

**Vídeo SEM reprojeção das BBs**: [Vídeo 3](https://drive.google.com/open?id=1q0VhpRobT0QBBMwNilO_FZfs_S_ogfeF);

**Vídeo COM reprojeção das BBs**: [Vídeo 4](https://drive.google.com/open?id=1oVYj75yWRZZ_MKG7ePeNaBvaYubrNR9M).

# EXPERIMENTO 3 #
# **3.3.2.3 Mapa de ocupação cumulativo do ambiente** #

**Vídeo SEM reprojeção das janelas de detecção BBs**: [Vídeo 5](https://drive.google.com/open?id=19-swp9dDSvSSpe4AVzg3ab6OXyY3HhKT);

Esse vídeo inclui o mapa de ocupação probabilístico. Esse mapa é o mesmo para o vídeo abaixo. Por isso não é exibido novamente.

**Vídeo COM reprojeção das janelas de detecção BBs**: [Vídeo 6](https://drive.google.com/open?id=1qtx8AYUwZt9gIFdIj50CrfIbyix4L4kR).
 